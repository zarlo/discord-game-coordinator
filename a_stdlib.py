import socket
from console import fg, bg, fx

def InternetCheck():
    ip = socket.gethostbyname(socket.gethostname())
    if ip=="127.0.0.1":
        return False
    else:
        return True

def Error(string):
    print(fg.red, string, fg.white)

def Clear():
    print("\033c")