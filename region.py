regionlist = []
class BaseRegion:
    RegionName = None
    RegionID = None
    RegionEmoji = None

    #Statistics
    RegionPlayerCount = 0
    FullServers = 0
    ConnectedServers = 0
    FailServers = 0

    def __init__(self, name, id, emoji):
        self.RegionName = name
        self.RegionID = id
        self.RegionEmoji = emoji

        regionlist.append(self)

    def __str__(self):
        return f"({self.RegionName}, {self.RegionID})"

region1 = BaseRegion("usa", 1, '🇺🇸')
region2 = BaseRegion("europe", 2, '🇪🇺')
region3 = BaseRegion("australia", 3, '🇦🇺')
region4 = BaseRegion("brazil", 4, '🇧🇷')
region5 = BaseRegion("russia", 5, '🇷🇺')
region6 = BaseRegion("singapore", 6, '🇸🇬')

regions = {
    "usa": region1,
    "na": region1,
    "america": region1,
    "us": region1,

    "europe": region2,
    "eur": region2,
    "westeurope": region2,
    "eu": region2,

    "aus": region3,
    "aussie": region3,

    "brazil": region4,
    "sa": region4,

    "russia" : region5,
    "westrussia": region5,
    "rus": region5,
    "ru": region5,

    "singapore": region6,
    "asia": region6,
}

def CalculateRegion(string):
    if string in regions:
        print("yes")
        return regions.get(string)
    else:
        return None